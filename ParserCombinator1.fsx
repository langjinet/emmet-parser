open System

// let parseA str =
//     if String.IsNullOrEmpty(str) then
//         (false, "")
//     else if str.[0] = 'A' then
//         let remaining = str.[1..]
//         (true, remaining)
//     else
//         (false, str)

let inputABC = "ABC"
// parseA inputABC
let inputZBC = "ZBC"
// parseA inputZBC

// Parsing a specified character
// let pchar (charToMatch, str) =
//     if String.IsNullOrEmpty(str) then
//         let msg = "No more input"
//         (msg, "")
//     else
//         let first = str.[0]
//         if first = charToMatch then
//             let remaining = str.[1..]
//             let msg = sprintf "Found %c" charToMatch
//             (msg,remaining)
//         else
//             let msg = sprintf "Expecting '%c'. Got '%c'" charToMatch first
//             (msg,str)

// pchar('A', inputABC)
// pchar('Z', inputABC)

// Returning a Success/Failure
type ParseResult<'a> =
    | Success of 'a
    | Failure of string

// let pchar charToMatch str =
//     if String.IsNullOrEmpty(str) then
//         Failure "No more input"
//     else
//         let first = str.[0]
//         if first = charToMatch then
//             let remaining = str.[1..]
//             Success (char,remaining)
//         else
//             let msg = sprintf "Expecting '%c'. Got '%c'" charToMatch first
//             Failure msg

// let parseA2 = pchar 'A'
// parseA2 inputABC

// Rewriting with an inner function
// let pchar charToMatch =
//     // define a nested inner function
//     let innerFn str = 
//         if String.IsNullOrEmpty(str) then
//             Failure "No more input"
//         else
//             let first = str.[0]
//             if first = charToMatch then
//                 let remaining = str.[1..]
//                 Success (charToMatch,remaining)
//             else
//                 let msg = sprintf "Expecting '%c'. Got '%c'" charToMatch first
//                 Failure msg
//     // return the inner function
//     innerFn

// let parseA = pchar 'A'

// Encapsulating the parsing function in a type
type Parser<'T> = Parser of (string -> ParseResult<'T * string>)

let pchar charToMatch =
    // define a nested inner function
    let innerFn str = 
        if String.IsNullOrEmpty(str) then
            Failure "No more input"
        else
            let first = str.[0]
            if first = charToMatch then
                let remaining = str.[1..]
                Success (charToMatch,remaining)
            else
                let msg = sprintf "Expecting '%c'. Got '%c'" charToMatch first
                Failure msg
    // return the inner function
    Parser innerFn

// can't run following code with parser
let parseA = pchar 'A'
// parseA inputABC

// define a runner for Parser
let run parser input = 
    // unwrap parser to get inner function
    let (Parser innerFn) = parser
    // call inner function with input
    innerFn input

run parseA inputABC


// Combining two parsers in sequence
let parseB = pchar 'B'
// let parseAThenB = parseA >> parseB
// above combine function doesn't work, need an anThen function

let andThen parser1 parser2 =
    let innerFn input =
        // run parser1 with the input
        let result1 = run parser1 input
        match result1 with
        | Failure err ->
            Failure err
        | Success (value1,remaining1) ->
            // run parser 2 with the remaining input
            let result2 = run parser2 remaining1
            // test the result
            match result2 with
            | Failure err ->
                Failure err
            | Success (value2, remaining2) ->
                // combine both values as a pair
                let newValue = (value1, value2)
                Success(newValue, remaining2)
    Parser innerFn

// define an infix version of andThen
let ( .>>. ) = andThen

let parserAThenB = parseA .>>. parseB
run parserAThenB "ABC"


// Choosing between two parsers
let orElse parser1 parser2 =
    let innerFn input =
        let result1 = run parser1 input

        match result1 with
        | Success result ->
            result1
        | Failure err ->
            let result2 = run parser2 input
            result2
    Parser innerFn

let ( <|> ) = orElse

// testing orElse
let parseAOrElseB = parseA <|> parseB
run parseAOrElseB "AZZ"
run parseAOrElseB "BZZ"
run parseAOrElseB "CZZ"


// combining "andThen" and "orElse"
let parseC = pchar 'C'
let bOrElseC = parseB <|> parseC
let aAndThenBorC = parseA .>>. bOrElseC

run aAndThenBorC "ABZ"
run aAndThenBorC "ACZ"
run aAndThenBorC "QBZ"
run aAndThenBorC "AQZ"

// Choosing from a list of parsers
let choice listOfParsers =
    List.reduce ( <|> ) listOfParsers

let anyOf listOfChars =
    listOfChars
    |> List.map pchar
    |> choice

let parseLowercase = 
    anyOf ['a'..'z']

let parseDigit =
    anyOf ['0'..'9']

run parseLowercase "aBC"
run parseLowercase "ABC"

run parseDigit "1ABC"
run parseDigit "9ABC"
run parseDigit "|ABC"