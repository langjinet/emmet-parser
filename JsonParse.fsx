#load "ParserLibrary.fsx"

open System
open ParserLibrary

type JValue =
    | JString of string
    | JNumber of float
    | JBool of bool
    | JNull
    | JObject of Map<string, JValue>
    | JArray of JValue list

// applier the parser p, ignore the result,
// and returns x.
let (>>%) p x =
    p |>> (fun _ -> x)

let jNull = 
    pstring "null"
    >>% JNull
    <?> "null"

run jNull "null"
run jNull "nulp" |> printResult

// parsing bool
let jBool =
    let jtrue =
        pstring "true"
        >>% JBool true      // map to JBool
    let jfalse =
        pstring "false"
        >>% JBool false     // map to JBool
    jtrue <|> jfalse
    <?> "bool"

run jBool "true"
run jBool "1" |> printResult

// parsing string
let jUnescapedChar =
    let label = "char"
    satisfy (fun ch -> ch <> '\\' && ch <> '\"') label

let jEscapedChar =
    [
        // (stringToMatch, resultChar)
        ("\\\"",'\"')      // quote
        ("\\\\",'\\')      // reverse solidus
        ("\\/",'/')        // solidus
        ("\\b",'\b')       // backspace
        ("\\f",'\f')       // formfeed
        ("\\n",'\n')       // newline
        ("\\r",'\r')       // cr
        ("\\t",'\t')       // tab
    ]
    |> List.map (fun (toMatch,result) ->
        pstring toMatch >>% result)
    |> choice
    <?> "escaped char"

let jUnicodeChar =
    let backslash = pchar '\\'
    let uChar = pchar 'u'
    let hexdigit =
        anyOf (['0'..'9'] @ ['A'..'F'] @ ['a'..'f'])
    let fourHexDigits =
        hexdigit .>>. hexdigit .>>. hexdigit .>>. hexdigit
    let convertToChar (((h1,h2),h3),h4) =
        let str = sprintf "%c%c%c%c" h1 h2 h3 h4
        Int32.Parse(str,Globalization.NumberStyles.HexNumber) |> char
    backslash >>. uChar >>. fourHexDigits
    |>> convertToChar

let quotedString =
    let quote = pchar '\"' <?> "quote"
    let jchar = jUnescapedChar <|> jEscapedChar <|> jUnicodeChar
    quote >>. manyChars jchar .>> quote

let jString =
    quotedString
    |>> JString
    <?> "quoted string"

// Parsing Number
let jNumber =
    let optSign = opt (pchar '-')
    let zero = pstring "0"
    let digitOneNine =
        satisfy (fun ch -> Char.IsDigit ch && ch <> '0') "1-9"
    let digit =
        satisfy (fun ch -> Char.IsDigit ch) "digit"
    let point = pchar '.'
    let e = pchar 'e' <|> pchar 'E'
    let optPlusMinus = opt (pchar '-' <|> pchar '+')
    let nonZeroInt =
        digitOneNine .>>. manyChars digit
        |>> fun (first,rest) -> string first + rest
    let intPart = zero <|> nonZeroInt
    let fractionPart = point >>. manyChars1 digit
    let exponentPart = e >>. optPlusMinus .>>. manyChars1 digit

    // utility function to convert an optional value to a string, or "" if missing
    let ( |>? ) opt f =
        match opt with
        | None -> ""
        | Some x -> f x

    let convertToJNumber (((optSign,intPart),fractionPart),expPart) =
        let signStr =
            optSign
            |>? string

        let fractionPartStr =
            fractionPart
            |>? (fun digits -> "." + digits)
        
        let expPartStr =
            expPart
            |>? fun (optSign, digits) ->
                let sign = optSign |>? string
                "e" + sign + digits
        (signStr + intPart + fractionPartStr + expPartStr)
        |> float
        |> JNumber
    
    optSign .>>. intPart .>>. opt fractionPart .>>. opt exponentPart
    |>> convertToJNumber
    <?> "number"

let jNumber_ = jNumber .>> spaces1

// Parsing Array
let createParserForwaredToRef<'a>() =
    let dummyParser : Parser<'a> =
        let innerFn _ = 
            failwith "unfixed forwared parser"
        {parseFn=innerFn; label="unknow"}
    let parserRef = ref dummyParser
    // wrapper Parser
    let innerFn input =
        runOnInput !parserRef input
    let wrapperParser = {parseFn=innerFn; label="unknown"}
    wrapperParser, parserRef
let jValue,jValueRef = createParserForwaredToRef<JValue>()
let jArray =
    let left = pchar '[' .>> spaces
    let right = pchar ']' .>> spaces
    let comma = pchar ',' .>> spaces
    let value = jValue .>> spaces
    // set up the list parser
    let values = sepBy value comma

    // set up the main parser
    between left values right
    |>> JArray
    <?> "array"

// Parsing object
let jObject =
    let left = spaces >>. pchar '{' .>> spaces
    let right = pchar '}' .>> spaces
    let colon = pchar ':' .>> spaces
    let comma = pchar ',' .>> spaces
    let key = quotedString .>> spaces
    let value = jValue  .>> spaces
    // set up the list parser
    let keyValue = (key .>> colon) .>>. value
    let keyValues = sepBy keyValue comma
    // set up the main parser
    between left keyValues right
    |>> Map.ofList
    |>> JObject
    <?> "object"

jValueRef := choice
    [
        jNull
        jBool
        jNumber
        jString
        jArray
        jObject
    ]

/// all done, testing time
let example1 = """{
  "name" : "Scott",
  "isMale" : true,
  "bday" : {"year":2001, "month":12, "day":25 },
  "favouriteColors" : ["blue", "green"],
  "emptyArray" : [],
  "emptyObject" : {}
}"""
// run jValue example1 |> printResult
// run jBool "true"
// run jObject """{ "a":1, "b"  :  2 }""" |> printResult
// run jObject """{ "a":1, "b"  :  true }"""
