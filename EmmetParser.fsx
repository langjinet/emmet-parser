#load "ParserLibrary.fsx"

open System
open ParserLibrary

type HValue =
    | HTag of string
    | HTagAttrClasses of (string * HValue) * HValue
    | HClass of string list
    | HAttribute of Map<string, string>
    | HString of string
    | HChilren of HValue list

let EmmetOperators =
    anyOf (['+';'/';'*'; ','])

let TagChar =
    anyOf (['0'..'9'] @ ['A'..'Z'] @ ['a'..'z'])

let htmlTag =
    let dot = pchar '.'
    let tagName = manyChars TagChar
    let className = manyChars TagChar
    tagName .>> dot >>. className
    |>> HTag
    <?> "html tag"

let hClass =
    let dot = pchar '.'
    let classname = manyChars TagChar
    let values = sepBy classname dot
    values
    |>> List.filter (fun c -> c <> "")
    |>> HClass
    <?> "classes"

let hAttrs =    
    let equal = pchar '='
    let delimeter = pchar ' '
    let akey = manyChars TagChar
    let aval = manyChars TagChar

    let left = pchar '['
    let right = pchar ']'
    let keyValue = (akey .>> equal) .>>. aval 
    
    let values = sepBy keyValue delimeter
    between left values right
    |>> Map.ofList
    |>> HAttribute
    <?> "attribute list"


let hElement =
    let tag = manyChars TagChar
    let classes = hClass
    let attrs = hAttrs

    let combined = tag .>>. classes .>>. attrs
    combined
    |>> HTagAttrClasses
    <?> "test"

run htmlTag "div.title.test"
run hClass ".title.test"
run hElement "div.title.test"
run hElement "h2.title"

let rec ToHtml result =
    let renderClasses classes =
        match classes with
        | HClass clList -> 
            match clList with
            | [] -> ""
            | l -> " class=\"" + (clList |> String.concat " ") + "\""
        | _ -> ""  // ignore all other cases

    let renderAttrs payload =
        match payload with
        | HAttribute attrs -> 
            attrs
            |> Seq.map (fun p -> p.Key + "=\"" + p.Value + "\"" )
            |> String.concat " "
            |> (+) " "
        | _ -> ""

    match result with
    | Success (value, input) ->
        match value with
        | HTag tag ->
            "<" + tag + ">"
        | HTagAttrClasses ((t, classes), attrs) -> 
            "<" + t + (renderClasses classes) + (renderAttrs attrs) + ">"
        | HClass c ->
            "c"
        | HAttribute a -> 
            "a"
        | HString s ->
            "s"
        | HChilren children ->
            "hcild"
    | Failure (label,error,parserPos) ->
        sprintf "failed %s" label

let r1 = (run hElement "h2.title.primary")
let r2 = (run hElement "h2")
r1 |> printResult
r1 |> ToHtml
r1 |> ToHtml |> printfn "Html: %s"
r2 |> ToHtml
let r3 = (run hElement "h2.title.primary[a=b c=1]")
r3 |> ToHtml