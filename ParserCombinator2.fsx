open System

/// Type that represents Success/Failure in parsing
type ParseResult<'a> =
    | Success of 'a
    | Failure of string

/// Type that wraps a parsing function
type Parser<'T> = Parser of (string -> ParseResult<'T * string>)

/// Parse a single character
let pchar charToMatch =
    // define a nested inner function
    let innerFn str =
        if String.IsNullOrEmpty(str) then
            Failure "No more input"
        else
            let first = str.[0]

            if first = charToMatch then
                let remaining = str.[1..]
                Success(charToMatch, remaining)
            else
                let msg =
                    sprintf "Expecting '%c'. Got '%c'" charToMatch first

                Failure msg
    // return the "wrapped" inner function
    Parser innerFn

/// Run a parser with some input
let run parser input =
    // unwrap parser to get inner function
    let (Parser innerFn) = parser
    // call inner function with input
    innerFn input

/// Combine two parsers as "A andThen B"
let andThen parser1 parser2 =
    let innerFn input =
        // run parser1 with the input
        let result1 = run parser1 input

        // test the result for Failure/Success
        match result1 with
        | Failure err ->
            // return error from parser1
            Failure err

        | Success (value1, remaining1) ->
            // run parser2 with the remaining input
            let result2 = run parser2 remaining1

            // test the result for Failure/Success
            match result2 with
            | Failure err ->
                // return error from parser2
                Failure err

            | Success (value2, remaining2) ->
                // combine both values as a pair
                let newValue = (value1, value2)
                // return remaining input after parser2
                Success(newValue, remaining2)

    // return the inner function
    Parser innerFn

/// Infix version of andThen
let (.>>.) = andThen

/// Combine two parsers as "A orElse B"
let orElse parser1 parser2 =
    let innerFn input =
        // run parser1 with the input
        let result1 = run parser1 input

        // test the result for Failure/Success
        match result1 with
        | Success result ->
            // if success, return the original result
            result1

        | Failure err ->
            // if failed, run parser2 with the input
            let result2 = run parser2 input

            // return parser2's result
            result2

    // return the inner function
    Parser innerFn

/// Infix version of orElse
let (<|>) = orElse

/// Choose any of a list of parsers
let choice listOfParsers = List.reduce (<|>) listOfParsers

/// Choose any of a list of characters
let anyOf listOfChars =
    listOfChars
    |> List.map pchar // convert into parsers
    |> choice


//////////////////////////////////////////////////////
////////// Above code from previous step /////////////
//////////////////////////////////////////////////////


// let pstring str =
//     str
//     |> Seq.map pchar
//     |> Seq.reduce andThen
let parseDigit = anyOf [ '0' .. '9' ]

let parseThreeDigits =
    parseDigit .>>. parseDigit .>>. parseDigit

run parseThreeDigits "1234A"

let mapP f parser =
    let innerFn input =
        // run parser with the input
        let result = run parser input
        // test the result for Failure/Success
        match result with
        | Success (value, remaining) ->
            // if success, return the value transformed by f
            let newValue = f value
            Success(newValue, remaining)
        | Failure err ->
            // if failed, return the error
            Failure err
    // return the inner function
    Parser innerFn

let (<!>) = mapP
let (|>>) x f = mapP f x

let parseThreeDigitsAsStr =
    let tupleParser =
        parseDigit .>>. parseDigit .>>. parseDigit
    // create a function that turns the tuple into a string
    let transformTuple ((c1, c2), c3) = System.String [| c1; c2; c3 |]
    mapP transformTuple tupleParser

run parseThreeDigitsAsStr "123A"

let parseThreeDigitsAsInt = mapP int parseThreeDigitsAsStr

run parseThreeDigitsAsInt "123A"

// Lifting functions to the world of Parsers
let returnP x =
    let innerFn input = Success(x, input)
    Parser innerFn

let applyP fP xP =
    (fP .>>. xP) |> mapP (fun (f, x) -> f x)

let (<*>) = applyP

let lift2 f xP yP = returnP f <*> xP <*> yP

// test lift2
let addP = lift2 (+)
let startsWith (str: string) (prefix: string) = str.StartsWith(prefix)

let startsWithP = lift2 startsWith

// Turning a list of Parsers into a single Parser
let rec sequence parserList =
    // define  the cons function, which is a two parameter function
    let cons head tail = head :: tail
    // lift it to Parser world
    let consP = lift2 cons

    match parserList with
    | [] -> returnP []
    | head :: tail -> consP head (sequence tail)

// test
let parsers = [ pchar 'A'; pchar 'B'; pchar 'C' ]
let combined = sequence parsers
run combined "ABCD"


// implementing the "pstring" parser
let charListToStr charList =
    charList |> List.toArray |> System.String

let pstring str =
    str
    // convert to list of char
    |> List.ofSeq
    // map each char to a pchar
    |> List.map pchar
    // convert to Parser<char list>
    |> sequence
    // convert Parser<char list> to Parser<string>
    |> mapP charListToStr

// test it
let parseABC = pstring "ABC"
run parseABC "ABCDE"
run parseABC "A|BCDE"
run parseABC "AB|DE"

// 4. Match a parser multiple times
let rec parseZeroOrMore parser input =
    let firstResult = run parser input

    match firstResult with
    | Failure err -> ([], input)
    | Success (firstValue, inputAfterFirstParse) ->
        let (subsequentValues, remainingInput) =
            parseZeroOrMore parser inputAfterFirstParse

        let values = firstValue :: subsequentValues
        (values, remainingInput)

let many parser =
    let innerFn input =
        Success (parseZeroOrMore parser input)
    Parser innerFn

let many1 parser =
    let innerFn input =
        let firstResult = run parser input
        match firstResult with
        | Failure err ->
            Failure err
        | Success (firstValue,inputAfterFirstParse) ->
            let (subsequentValues,remainingInput) = parseZeroOrMore parser inputAfterFirstParse
            let values = firstValue::subsequentValues
            Success(values, remainingInput)
    Parser innerFn

// Parsing an integer
let pint1 =
    let resultToInt digitList =
        digitList |> List.toArray |> System.String |> int
    let digit = anyOf['0'..'9']
    let digits = many1 digit
    digits
    |> mapP resultToInt

run pint1 "12ABC"
run pint1 "1ABC"

// 5. matching a parser zero or one time
let opt p =
    let some = p |>> Some
    let none = returnP None
    some <|> none

let digit = anyOf['0'..'9']
let digitThenSemicolon = digit .>>. opt (pchar ';')

run digitThenSemicolon "1;"
run digitThenSemicolon "1"

let pint =
    let resultToInt (sign,charList) =
        let i = charList |> List.toArray |> System.String |> int
        match sign with
        | Some ch -> -i
        | None -> i
    let digit = anyOf['0'..'9']
    let digits = many1 digit

    opt(pchar '-') .>>. digits
    |>> resultToInt

run pint "123C"
run pint "-123C"

// 6. Throwing results away
let (.>>) p1 p2 =
    p1 .>>. p2
    |> mapP (fun (a,b) -> a)

let (>>.) p1 p2 =
    p1 .>>. p2
    |> mapP (fun (a,b) -> b)

let between p1 p2 p3 =
    p1 >>. p2 .>> p3

// 7. Parsing lists with separators
let sepBy1 p sep =
    let sepThenP = sep >>. p
    p .>>. many sepThenP
    |>> fun (p,pList) -> p::pList

let sepBy p sep =
    sepBy1 p sep <|> returnP []

let comma = pchar ','
let zeroOrMoreDigitList = sepBy digit comma
let oneOrMoreDigitList = sepBy1 digit comma

let bindP f p =
    let innerFn input =
        let result1 = run p input
        match result1 with
        | Failure err ->
            Failure err
        | Success (value1,remainingInput) ->
            let p2 = f value1
            run p2 remainingInput
    Parser innerFn

let ( >>= ) p f = bindP f p

// then can reimplement other combinators using "bind"
let mapP2 f =
    bindP (f >> returnP)

let andThen2 p1 p2 =
    p1 >>= (fun p1Result ->
    p2 >>= (fun p2Result ->
        returnP (p1Result, p2Result) ))

let applyP2 fP xP =
    fP >>= (fun f ->
    xP >>= (fun x ->
        returnP (f x) ))

// defined many1 based on many & bind
let manyOne p =
    p       >>= (fun head ->
    many p  >>= (fun tail ->
        returnP (head::tail)))




//////// Here is my thoughts
// implement a all function based on andThen
// any: based on orElse



// (*) .>>. (andThen) applies the two parsers in sequence and returns the results in a tuple.
// (*) <|> (orElse) applies the first parser, and if that fails, the second parsers.
// (*) choice extends orElse to choose from a list of parsers.

// And in this post we created the following additional combinators:

// (*) bindP chains the result of a parser to another parser-producing function.
// (*) mapP transforms the result of a parser.
// (*) returnP lifts an normal value into the world of parsers.
// (*) applyP allows us to lift multi-parameter functions into functions that work on Parsers.
// (*) lift2 uses applyP to lift two-parameter functions into Parser World.
// (*) sequence converts a list of Parsers into a Parser containing a list.
// (*) many matches zero or more occurrences of the specified parser.
// (*) many1 matches one or more occurrences of the specified parser.
// (*) opt matches an optional occurrence of the specified parser.
// (*) .>> keeps only the result of the left side parser.
// (*) >>. keeps only the result of the right side parser.
// (*) between keeps only the result of the middle parser.
// (*) sepBy parses zero or more occurrences of a parser with a separator.
// (*) sepBy1 parses one or more occurrences of a parser with a separator.
